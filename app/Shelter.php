<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Worker;
class Shelter extends Model
{
    //
    protected $fillable = [ 'uskey', 'name', 'city', 'size'];

    public function workers() {
        return $this->belongsToMany(Worker::class);
    }

    public function cats() {
        return $this->belongsToMany(Cat::class);
    }

    public static function generateUSKey() {
        $length             = 5;
        $characters         = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength   = strlen($characters);
        $randomString       = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
