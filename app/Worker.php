<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    //
    protected $fillable = [ 'name', 'age'];

    public function cats() {
        return $this->belongsToMany(Cat::class);
    }
}
