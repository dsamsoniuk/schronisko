<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cat;

class CatController extends Controller
{
    //
    public function index() {

        $cats = Cat::all(['name','color'])->toArray();

        if (empty($cats)) {
            $cats = [
                "msg" => "Not found cats"
            ];
        } 

        return response()->json($cats);
    }

    public function findOne(int $id) {

        $cats = [];
        if ($id > 0) {
            $cats = Cat::select(['name','color'])->where('id', $id)->get()->first()->toArray();
        }
        
        if (empty($cats)) {
            $cats = [
                "msg" => "Not found cat"
            ];
        } 

        return response()->json($cats);
    }
}
