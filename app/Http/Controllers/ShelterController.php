<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shelter;

class ShelterController extends Controller
{

   public function index() {

    $shelters = Shelter::all(['name','city','size'])->sortBy('city')->take(2);

    if ($shelters->isEmpty()) {
        $shelters= [
            "msg" => "Not found shelters"
        ];
    } else {
        $shelters= array_values($shelters->toArray());
    }
   return  response()->json($shelters);
}

    public function findOne(string $uskey) {

        $shelters = Shelter::select(['name','city','size'])->where('uskey', $uskey )->get()->first();
        
        if (empty($shelters)) {
            $shelters = [
                "msg" => "Not found shelter"
            ];
        } else {
            $shelters = $shelters->toArray();
        }

        return response()->json($shelters);
    }
}
