<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Worker;

class Cat extends Model
{
    //
    protected $fillable = [ 'name', 'color'];

    public function guardian() {
        
        return $this->belongsTo(Worker::class, 'id');
    }

    public function shelter() {
        return $this->belongsTo('App\Shelter','id');
    }

}
